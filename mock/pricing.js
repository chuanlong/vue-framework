import Mock from 'mockjs'

const records = [{
  typeName: '审核通过',
  content: '“审核通过”',
  operator: '大虫|dachogn001',
  operatatedTime: '2019-12-31 14:28:45'
}, {
  typeName: '编辑',
  content: '编辑',
  operator: '刘毅|liuyi001',
  operatatedTime: '2019-12-31 14:28:45'
}, {
  typeName: '停用',
  content: '停用',
  operator: '远辰|yuanchen041',
  operatatedTime: '2019-12-31 14:28:45'
}, {
  typeName: '审核不通过',
  content: '“请重新修改价格”',
  operator: '远辰|yuanchen041',
  operatatedTime: '2019-12-31 14:28:45'
}, {
  typeName: '配额调整',
  content: '配额调整',
  operator: '远辰|yuanchen041',
  operatatedTime: '2019-12-31 14:28:45'
}, {
  typeName: '启用',
  content: '启用',
  operator: '远辰|yuanchen041',
  operatatedTime: '2019-12-31 14:28:45'
}]

const detail = {
  price: {
    tickets: [
      {
        id: '300',
        name: '成人票',
        unit: '元'
      },
      {
        id: '301',
        name: '亲子票',
        unit: '元'
      },
      {
        id: '302',
        name: '团体票',
        unit: '元/人'
      },
      {
        id: '303',
        name: '携程特价票',
        unit: '元'
      }
    ],
    items: [
      {
        groupId: '100',
        groupName: 'OTA渠道',
        channelId: '200',
        channelName: '携程',
        tickets: [
          {
            id: '300',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '301',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '302',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '303',
            price: Mock.mock('@integer(100, 400)')
          }
        ]
      },
      {
        groupId: '100',
        groupName: 'OTA渠道',
        channelId: '201',
        channelName: '去哪儿',
        tickets: [
          {
            id: '300',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '301',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '302',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '303',
            price: Mock.mock('@integer(100, 400)')
          }
        ]
      },
      {
        groupId: '101',
        groupName: '自有渠道',
        isUnified: true,
        tickets: [
          {
            id: '300',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '301',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '302',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '303',
            price: Mock.mock('@integer(100, 400)')
          }
        ]
      },
      {
        groupId: '102',
        groupName: '旅行社渠道',
        isUnified: true,
        tickets: [
          {
            id: '300',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '301',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '302',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '303',
            price: Mock.mock('@integer(100, 400)')
          }
        ]
      },
      {
        groupId: '103',
        groupName: '线下门店渠道',
        isUnified: true,
        tickets: [
          {
            id: '300',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '301',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '302',
            price: Mock.mock('@integer(100, 400)')
          },
          {
            id: '303',
            price: Mock.mock('@integer(100, 400)')
          }
        ]
      }]
  },
  quota: {
    tickets: [
      {
        id: '300',
        name: '成人票',
        unit: '人'
      },
      {
        id: '301',
        name: '亲子票',
        unit: '人'
      },
      {
        id: '302',
        name: '团体票',
        unit: '人'
      },
      {
        id: '303',
        name: '携程特价票',
        unit: '人'
      }
    ],
    items: [
      {
        groupId: '100',
        groupName: '1000万以上组',
        channelId: '200',
        channelName: '携程',
        tickets: [
          {
            id: '300',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '301',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '302',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '303',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          }
        ]
      },
      {
        groupId: '100',
        groupName: '1000万以上组',
        channelId: '201',
        channelName: '去哪儿',
        tickets: [
          {
            id: '300',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '301',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '302',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '303',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          }
        ]
      },
      {
        groupId: '101',
        groupName: '300万到500万组',
        tickets: [
          {
            id: '300',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '301',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '302',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '303',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          }
        ]
      },
      {
        groupId: '102',
        groupName: '300万一下组',
        tickets: [
          {
            id: '300',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '301',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '302',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          },
          {
            id: '303',
            type: Mock.mock('@integer(0, 3)'),
            limit: 100,
            total: 500
          }
        ]
      }
    ]
  }
}

export default [
  // pricing list
  {
    url: '/vue-element-admin/pricing/list',
    type: 'post',
    response: config => {
      const { body } = config
      const { pageNum, pageSize } = body
      const max = pageNum > 4 ? pageSize - 3 : pageSize
      const total = 4 * pageSize + max

      const list = []
      for (let i = 0; i < max; i++) {
        list.push({
          id: `1${pageNum}${i}`,
          schemeNo: `${Mock.mock('@integer(1000, 5000)')}`,
          schemeName: `童话世界平日价格方案_${pageNum}_${i}`,
          pricingItemName: '景点产品-海花岛童话世界',
          validStartTime: '2020.01.22',
          validEndTime: '2020.02.01',
          priority: '/',
          status: Mock.mock('@integer(1, 7)'),
          records
        })
      }

      return {
        code: 20000,
        data: {
          list,
          total,
          pages: 5
        }
      }
    }
  },

  // get pricing detail
  {
    url: '/vue-element-admin/pricing/detail',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: detail
      }
    }
  }
]
