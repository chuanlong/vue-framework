
import Mock from 'mockjs'

export default [
  {
    url: '/scenic/queryList', // /vue-element-admin/merchant/info/list
    type: 'post',
    response: config => {
      const { body } = config
      const { pageNum, pageSize } = body
      const max = pageNum > 4 ? pageSize - 3 : pageSize
      const total = 4 * pageSize + max

      const list = []
      for (let i = 0; i < max; i++) {
        const creditBalance = Mock.mock('@float(0, 10000)')
        let mobile1 = `${Mock.mock('@float(0.11)')}`
        mobile1 = mobile1.replace('0.', '')
        const obj = {
          id: `1${pageNum}${i}`,
          scenicName: Mock.mock('@cparagraph(1)'),
          accountBalance: Mock.mock('@float(0, 10000)'),
          creditBalance,
          creditTotal: Mock.mock(`@float(${creditBalance}, ${creditBalance + 10000})`),
          currentApplicant: Mock.mock('@word'),
          officeStatus: Mock.mock('@integer(1, 2)'),
          createdTime: Mock.mock('@datetime'),
          contact1: Mock.mock('@word'),
          mobile1
        }

        list.push(obj)
      }

      return {
        resultCode: 0,
        data: {
          list,
          total,
          pages: 5
        }
      }
    }
  }
]
