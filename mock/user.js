
import utilConfig from '../src/utils/config'
const tokens = {
  admin: {
    username: 'admin',
    pwd: '123456'
  },
  editor: {
    username: 'editor',
    pwd: '123456'
  }
}

export default [
  {
    url: '/auth/verifyCode',
    type: 'get',
    response: _ => {
      return {
        resultCode: 0,
        data: {
          image: '/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAAeAFoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3ueeG1t5bi4ljhgiQvJJIwVUUDJJJ4AA5zVebVtNttLGqT6haRaeUVxdvMqxFWxtO8nGDkYOeciqLeD/DDKVbw5pBBGCDYxc/+O14bbWkmoeBtN8G3s13NqH/AAkv9g3XlajPLvt4WEkksURO0LGuxclCFABIBIxXu9yvdPoCHVtNudLOqQahaS6eEZzdpMrRBVzuO8HGBg5OeMGuX8d+K9S0bRtDuPDZ025n1fU7eyhkutzwFZVYq4KEHGQpyM8E8GvJ7m0k0/wNqXg2ymu4dQ/4SX+wbXzdRni2W8zGSOWWIHaVkXeuQgDAkgEg5634p+H5tJ0bwpa+Hb2a2Ya9Zw2VvcymWCGXa4jbLhnAGFG0Hbj+HNFl3Cy7nT2H/C0/7Rtv7R/4Q37D5qfaPs/2rzPLyN2zPG7GcZ4zXcV5tomi/Ff+2IP7f8UaX/Znzed9giXzvunbt3wbfvbc57ZrsP7L1iD5rbxFNM54K39rFImPUCIRNn3LEYzx0IfKu/5/5Byrv+f+Rka38UvBvhzWJ9J1bWfs99Bt8yL7LM+3coYcqhB4IPBqx4b+InhXxdqMlhoeq/a7qOIzMn2eWPCAgE5dQOrD8680XxhF4P8Air40XWfEOnWNxc/YfnbSp5ll2wfwqkmUwGGck5zxjpXdeGPGEni77U+geIdJ1L7HsM0X9lXFvndnau95DjO1hkK2OuD0K5V3DlXc7miuW1vXvE2iaPPqP/CM29/5O3/RrC+klmfLBflXyBnGcn2Brh774161plnJeX/w21u0tY8b5p98aLkgDLGLAySB+NPkf9NByP8Apo9hornrDxHqV3p1tcyeEtYheaJJGjaS2BQkAkENKrcdOQD6gdKsf21qH/Qr6t/39tP/AI/RyP8ApoOR/wBNGzXBx/DOEfFubx5LqkjOUxFZrCFCt5Ihyz5O4bdxwAOSOeOe8oqCTg5PhnCfi3D48i1SRXCYls2hDBm8kw5V8jaNu04IPIPPPGp408N3niP/AIR77HJAn9m63bahN5zEbo4924LgHLcjAOB711FFABRRRQB4/B428O+Dvi947/t/UPsf2r+z/J/cySbttv8AN9xTjG5evrXceG/iJ4V8XajJYaHqv2u6jiMzJ9nljwgIBOXUDqw/OuoooAr399b6Zp1zf3knl2trE80z7SdqKCWOBycAHpXmelaVffFTVLfxH4jtpLbwrbv5mkaNL1uj2uJx0II+6vQg91JMnomt6Jp3iPR59J1a3+0WM+3zIt7Ju2sGHKkEcgHg1x//AApL4ef9C9/5O3H/AMcoA9AoqvYWNvpmnW1hZx+Xa2sSQwpuJ2ooAUZPJwAOtWKAP//Z'
        }
      }
    }
  },
  // user login
  {
    url: '/auth/login',
    type: 'post',
    response: config => {
      const { username, password } = config.body

      const loginUser = tokens[username]

      // mock error
      if (!loginUser || password !== loginUser.pwd) {
        return {
          resultCode: -1,
          resultMsg: '账号或密码不正确'
        }
      }
      return {
        resultCode: utilConfig.successResultCode,
        data: {
          user: username,
          nickName: username,
          account: username,
          resourceIds: '1257198272342460494,1257198262528837655,1257198250017228822',
          token: 'eyJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50SWQiOiIxMjM3NjM4MTAwMzk2ODY3NjkxIiwidW5pcXVlSWQiOiIxYWNkYzk0ZGZlYzg0ODBhYmE1OGMzN2E2NmMwZTBlZSIsImxvZ2luU291cmNlIjoiYXBwbGljYXRpb24iLCJsb2dpblRpbWUiOjE2MDQ3MDg3NTYwMDAsImV4dEZpZWxkcyI6e30sImp0aSI6ImYxMzBkMzkwLTIwODItNDg4YS1iZjZiLTI2OTJlOTIyNDBiYyIsIm5iZiI6MTYwNDY3OTk1NiwiZXhwIjoxNjEyNDU1OTU2fQ.OjHGrXi7bLjjJc13ojhI5R96GiqhuBWVqga8rh0KvKc',
        }
      }
    }
  },

  {
    url: '/auth/logout',
    type: 'put',
    response: config => {
      return {
        resultCode: utilConfig.successResultCode,
        data: {}
      }
    }
  }
]
