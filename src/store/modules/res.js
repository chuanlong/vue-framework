import { getList } from '@/api/res'
import siteConfig from '@/utils/config'
import _ from 'lodash'

const actions = {
  // eslint-disable-next-line no-empty-pattern
  getList() {
    return new Promise((resolve, reject) => {
      const deptId = localStorage.getItem(siteConfig.localStorage.deptId)
      const groupId = localStorage.getItem(siteConfig.localStorage.groupId)
      const resourceIds = localStorage.getItem(siteConfig.localStorage.resourceIds)
      const arr = resourceIds ? resourceIds.split(',') : []
      const rootId = groupId || deptId

      getList({ rootId, status: 1 })
        .then(res => {
          const { data } = res

          const _findParents = (arg) => {
            let array = []

            if (!arg) {
              return array
            }

            // 资源类型,1-应用,2- q菜单,3-元素
            const { resourceType, parentId } = arg

            if ('' + resourceType === '1') {
              return array
            }

            array.push(arg)
            const index = data ? data.findIndex(item => item.id === parentId) : -1

            if (index < 0) {
              return array
            }

            const parent = data[index]
            array = array.concat(_findParents(parent))
            return array
          }

          let resources = []
          for (const resId of arr) {
            const index = data ? data.findIndex(item => item.id === resId) : -1
            if (index < 0) {
              continue
            }

            const resource = data[index]
            const array = _findParents(resource)
            resources = resources.concat(array)
          }

          resources = _.uniqBy(resources, 'id')
          console.log('xxxxx current logged in user has resources ===>', resources)
          localStorage.setItem(siteConfig.localStorage.resources, JSON.stringify(resources))
          resolve()
        })
        .catch(e => reject(e))
    })
  }
}

export default {
  namespaced: true,
  actions
}
