import { getTicketDetail, ticketAdd, ticketUpdate } from '@/api/ticket'

const state = {
  isNewAddTicket: false,
  detail: {},
  formValues: {}
}

const mutations = {
  MODIFY_STATUS: (state, val) => {
    state.isNewAddTicket = val
  },
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  },
  INIT_FORM_VALUES: function(state, initialVal) {
    state.formValues = initialVal || {}
  }
}

const actions = {
  async getTicketDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getTicketDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { specValueId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      commit('SET_STATE', { detail: obj })
      return obj
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },

  ticketAdd({ commit }, data) {
    return ticketAdd(data)
  },

  ticketUpdate({ commit }, data) {
    return ticketUpdate(data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

