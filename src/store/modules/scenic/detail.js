import { getScenicDetail, getCatalogs } from '@/api/scenic'

const state = {
  detail: {}
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getScenicDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getScenicDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { storeId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      commit('SET_STATE', { detail: obj })
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },
  getCatalogs() {
    return new Promise((resolve, reject) => {
      getCatalogs()
        .then(res => resolve(res.data))
        .catch(e => reject(e))
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

