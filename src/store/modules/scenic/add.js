import { getScenicDetail, scenicAdd, scenicUpdate, getCatalogs } from '@/api/scenic'

const state = {
  isNewAddScenic: false,
  detail: {},
  formValues: {}
}

const mutations = {
  MODIFY_STATUS: (state, val) => {
    console.log(val, 'mutations')
    state.isNewAddScenic = val
  },
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  },
  INIT_FORM_VALUES: function(state, initialVal) {
    state.formValues = initialVal || {}
  }
}

const actions = {
  async getScenicDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getScenicDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { storeId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      commit('SET_STATE', { detail: obj })
      return obj
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },

  scenicAdd({ commit }, data) {
    return scenicAdd(data)
  },

  scenicUpdate({ commit }, data) {
    return scenicUpdate(data)
  },

  getCatalogs() {
    return new Promise((resolve, reject) => {
      getCatalogs()
        .then(res => resolve(res.data))
        .catch(e => reject(e))
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
