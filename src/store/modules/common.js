import { getOrg, getMaterials, getSAPList } from '@/api/common'
import store from '@/store'

const state = {
  depList: [],
  depTreeData: [],
  regionList: [],
  regionTreeData: [],
  region2LevelTreeData: [],
  regionMap: {},
  materials: [],
  SAPList: []
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getOrg({ commit }) {
    try {
      const { resultCode, data } = await getOrg()
      if ('' + resultCode === '0') {
        const depList = data || []
        const depMap = {}
        const depTreeData = []

        const { permissions } = store.state['user']

        const newDepList = []
        for (const item of depList) {
          const index = permissions ? permissions.indexOf(item.id) : -1
          depMap[item.id] = item
          item.children = []
          if (!item.parentId) {
            depTreeData.push(item)// first level items
          }

          if (index < 0) {
            item.disabled = true
          } else {
            newDepList.push(item)
          }
        }

        for (const id in depMap) {
          const item = depMap[id]
          if (item.parentId) {
            depMap[item.parentId] && depMap[item.parentId].children.push(item)
          }
        }
        for (const id in depMap) {
          const item = depMap[id]
          if (item.children.length === 0) {
            delete item.children
          }
        }

        console.log('xxx depTreeData ==>', depTreeData)

        commit('SET_STATE', { depList: newDepList, depTreeData })
      }
    } catch (e) {
      console.log('get getOrg error:', e)
    }
  },
  async getMaterials({ commit }) {
    if (state.materials.length) {
      return state.materials
    }
    const params = {
      'werks': 1000,
      'dateFrom': '2019-01-01',
      'dateTo': '2099-12-31',
      'mtart': [{ 'mtart_f': 'Z003', 'mtart_t': 'Z003' }],
      'matkl': [{ 'matkl_f': '30010101', 'matkl_t': '30010101' }]
    }
    const { resultCode, data } = await getMaterials(params)
    if ('' + resultCode === '0') {
      commit('SET_STATE', { materials: data })
    }
  },
  async getSAPList({ commit }) {
    if (state.SAPList.length) {
      return state.SAPList
    }
    const params = { 'vkorg': '1000', 'dateFrom': '2019-01-10', 'dateTo': '2050-12-31', 'zxtbs': 'PWS', 'ktokd': '' }
    const { resultCode, data } = await getSAPList(params)
    if ('' + resultCode === '0') {
      commit('SET_STATE', { SAPList: data })
    }
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

