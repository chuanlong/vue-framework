import { getHotelDetail, createHotel, updateHotel } from '@/api/hotel'

const state = {
  detail: {},
  formValues: {
    facilities: [],
    enFacilities: []
  }
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getHotelDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getHotelDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { storeId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      obj.facilities = obj.facilities || []
      obj.enFacilities = []

      commit('SET_STATE', { detail: obj })
      return obj
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },

  createHotel({ commit }, data) {
    return createHotel(data)
  },

  updateHotel({ commit }, data) {
    return updateHotel(data)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

