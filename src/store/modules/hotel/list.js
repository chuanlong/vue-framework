
import { getHotelCrsCodes, getHotels } from '@/api/hotel'

const state = {
  crsCodeList: [],
  hotelList: {}
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getHotelCrsCodes({ commit }) {
    try {
      const { resultCode, data } = await getHotelCrsCodes()
      if ('' + resultCode === '0') {
        const crsCodeList = (data || []).filter(code => code).map(code => ({ label: code, value: code }))
        commit('SET_STATE', { crsCodeList })
        return crsCodeList
      }
    } catch (e) {
      console.log('get HotelCrsCodes error:', e)
    }
  },
  async getHotels({ commit }) {
    try {
      const { resultCode, data } = await getHotels()
      if ('' + resultCode === '0') {
        const hotelList = (data.list || []).map(hotel => ({ label: hotel.hotelName, value: hotel.id }))
        commit('SET_STATE', { hotelList })
      }
    } catch (e) {
      console.log('get getHotels error:', e)
    }
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

