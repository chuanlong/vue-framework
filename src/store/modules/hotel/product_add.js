import { getHotelProductDetail, addHotelProduct, updateHotelProduct } from '@/api/hotel'

const state = {
  detail: {},
  detailFetched: false
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getHotelProductDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getHotelProductDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { skuId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      if (obj.hotelDto) {
        obj.hotelId = obj.hotelDto.id
      }
      if (obj.layoutDto) {
        obj.houseLayoutId = obj.layoutDto.id
      }
      commit('SET_STATE', { detail: obj, detailFetched: true })
      return obj
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },
  addHotelProduct({ commit }, data) {
    return addHotelProduct(data)
  },
  updateHotelProduct({ commit }, data) {
    return updateHotelProduct(data)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

