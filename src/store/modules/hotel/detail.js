import { getHotelDetail, getRelatedHotelProducts, getRelatedHouseTypes } from '@/api/hotel'

const state = {
  detail: {
    facilities: [],
    enFacilities: []
  },
  relatedHouseTypes: [], // 关联的酒店房型
  relatedHotelProducts: [] // 关联的酒店产品
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getHotelDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getHotelDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { storeId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      const { facilities, enFacilities } = obj
      obj.facilities = facilities || []

      try {
        obj.enFacilities = enFacilities ? JSON.parse(enFacilities) : []
      } catch (e) {
        obj.enFacilities = []
        console.log('xxxxx enFacilities JSON.parse: error ==>', e)
      }

      commit('SET_STATE', { detail: obj })
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },
  async getRelatedHouseTypes({ commit }, id) {
    try {
      const { resultCode, data } = await getRelatedHouseTypes(id)
      if ('' + resultCode === '0') {
        commit('SET_STATE', { relatedHouseTypes: data.list })
      }
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },
  async getRelatedHotelProducts({ commit }, id) {
    try {
      const { resultCode, data } = await getRelatedHotelProducts(id)
      if ('' + resultCode === '0') {
        commit('SET_STATE', { relatedHotelProducts: data.list })
      }
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

