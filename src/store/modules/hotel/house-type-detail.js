import { getHouseTypeDetail, delRelateHotel } from '@/api/hotel'

const state = {
  detail: {}
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  async getHouseTypeDetail({ commit }, id) {
    try {
      const results = await Promise.all([
        getHouseTypeDetail(id)
      ])

      const obj = {}
      for (let i = 0; i < results.length; i++) {
        const result = results[i]
        let { data } = result || {}
        data = data || {}
        Object.assign(obj, data)

        if (i) {
          Object.assign(obj, { specValueId: data.id })
        } else {
          Object.assign(obj, { enId: data.id })
        }
      }

      commit('SET_STATE', { detail: obj })
      return obj
    } catch (e) {
      console.log('get Hotel Detail error:', e)
    }
  },

  delRelateHotel({ commit }, data) {
    return delRelateHotel(data)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

