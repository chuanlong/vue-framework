
import { houseTypeRelateHotel, getLayoutCrsCodes, getHotels } from '@/api/hotel'

const state = {
  detail: {},
  crsCodeList: [],
  hotels: []
}

const mutations = {
  SET_STATE: function(state, obj) {
    Object.assign(state, obj)
  }
}

const actions = {
  houseTypeRelateHotel({ commit }, data) { // 酒店房型关联酒店
    return houseTypeRelateHotel(data)
  },
  async getLayoutCrsCodes({ commit }) {
    let result = await getLayoutCrsCodes({ pageNum: 1, pageSize: 1000 })
    result = result || {}
    if ('' + result.resultCode === '0') {
      let data = result.data || []
      data = data.filter(crsCode => !!crsCode).map(code => ({ label: code, value: code }))
      commit('SET_STATE', { crsCodeList: data })
    }
  },
  async getAllHotel({ commit }) {
    const { resultCode, data } = await getHotels()
    const list = (data.list || []).map(hotel => ({ label: hotel.hotelName, value: hotel.id }))
    if ('' + resultCode === '0') {
      commit('SET_STATE', { hotels: list })
    }
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

