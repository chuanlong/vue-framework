import { constantRoutes } from '@/router'
import { findIndex, isNil, cloneDeep } from 'lodash'

export function initRoutes(routes, resources) {
  // const results = []
  // const routes1 = routes || []
  //
  // for (const route of routes1) {
  //   const { noPermCtrl, code, meta, children } = route
  //
  //   if (noPermCtrl) {
  //     results.push(route)
  //     continue
  //   }
  //
  //   if (!code) {
  //     continue
  //   }
  //
  //   const index = resources ? findIndex(resources, item => item.resourceCode === code) : -1
  //
  //   if (index < 0) {
  //     continue
  //   }
  //   const resource = resources[index]
  //   const { roleAddress, redirect, alwaysShow, resourceName, resourceIcon } = resource
  //
  //   route.path = roleAddress || route.path
  //   const meta1 = meta || {}
  //   Object.assign(meta1, { title: resourceName, icon: resourceIcon || meta.icon })
  //   route.meta = meta1
  //   !isNil(redirect) && Object.assign(route, { redirect })
  //   !isNil(alwaysShow) && Object.assign(route, { alwaysShow })
  //   route.children = initRoutes(children, resources)
  //   results.push(route)
  // }

  // return results
  return routes
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = routes
  }
}

const actions = {
  generateRoutes({ commit }, resources) {
    return new Promise(resolve => {
      const arr = cloneDeep(constantRoutes)
      const accessedRoutes = initRoutes(arr, resources)
      accessedRoutes.push({ path: '*', redirect: '/404', hidden: true })
      if (process.env.NODE_ENV === 'development') {
        accessedRoutes.push({
          path: '/resource/list',
          component: () => import('@/views/resource/index'),
          name: 'ResourceList',
          meta: { title: '资源列表', icon: 'iconicon_outline_menu_shouye1' }
        })
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
