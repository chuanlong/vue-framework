import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.less'

import './styles/index.less' // global css

import './assets/iconfont/iconfont.css'

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters
// 注册滚动条加载触发事件v-loadmore绑定

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */

Vue.config.devtools = true
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})
router.beforeEach((to, from, next) => {
  const { query, meta } = to
  const { __title, type } = query || {}
  query && delete query['__title']
  let title = meta.title || ''
  if (type === 'add') title = '新增' + title.slice(2)
  if (type === 'edit') title = '编辑' + title.slice(2)
  if (type === 'audit') title = '审核' + title.slice(2)
  if (type === 'detail') title = '查看' + title.slice(2)
  if (meta) {
    meta.title = __title || title
  } else {
    to.meta = { title: __title }
  }

  next()
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 注册一个全局自定义指令 不允许输入空格&*%等特殊字符
Vue.directive('filterSpecialChar', {
  update: function(el, { value, modifiers }) {
    try {
      const newValue = value
        ? value.replace(/[`~!~@#$%^&*()\+=<>?:"{}|,.\/;'\\[\]·~！~@#￥%……& *（）——\+={}|《》？：“”【】、；‘'，。、]/g, '')
        : ''
      if (value !== newValue) {
        el.children[0].value = newValue
        el.children[0].dispatchEvent(new Event(modifiers.lazy ? 'change' : 'input'))
      }
    } catch (e) {
      console.log('filterSpecialChar error ==>', e)
    }
  }
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
