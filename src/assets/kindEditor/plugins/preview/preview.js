/** *****************************************************************************
* KindEditor - WYSIWYG HTML Editor for Internet
* Copyright (C) 2006-2011 kindsoft.net
*
* @author Roddy <luolonghao@gmail.com>
* @site http://www.kindsoft.net/
* @licence http://www.kindsoft.net/license.php
*******************************************************************************/

KindEditor.plugin('preview', function(K) {
  var self = this; var name = 'preview'; var undefined
  self.clickToolbar(name, function() {
    var lang = self.lang(name + '.')
    var html = '<div style="padding:10px 20px;">' +
				'<iframe class="ke-textarea" frameborder="0" style="width:708px;height:400px;"></iframe>' +
				'</div>'
    var dialog = self.createDialog({
      name: name,
      width: 750,
      title: self.lang(name),
      body: html
    })
    var iframe = K('iframe', dialog.div)
    var doc = K.iframeDoc(iframe)
    doc.open()
    doc.write(self.fullHtml())
    doc.close()
    K(doc.body).css('background-color', '#FFF')
    iframe[0].contentWindow.focus()
  })
})
