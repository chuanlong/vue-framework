import moment from 'moment'

/**
 * cleanup undefined/null/''/whitespace
 * @param params
 * @param func
 * @returns {{}|null}
 */
export const clearParams = (params, func) => {
  if (!params) {
    return null
  }

  const obj = {}
  const keys = Object.keys(params)
  keys.forEach(key => {
    const val = params[key]
    let result
    if (func && typeof func === 'function') {
      result = func(val)
    } else {
      result = val === undefined || val === null || `${val}`.trim() === ''
    }
    if (!result) {
      obj[key] = val
    }
  })
  return obj
}

export const completeFullTime = (arg, type) => {
  const suffix = type ? '23:59:59' : '00:00:00'
  const result = formatDate(arg)

  return `${result} ${suffix}`
}

export const triggerDownload = (url, fileName) => {
  const a = document.createElement('a')
  a.href = url
  a.download = fileName
  document.body.append(a)
  a.click()
  a.remove()
}

/**
 * 格式化日期
 * @param arg
 *        string   'YYYY-MM'/'YYYY-MM-DD'/'YYYY-MM-DD hh:mm:ss'
 *        moment
 *        Date
 *        object  { year, month, day }
 * @param formatter
 * @returns {string|*[]}
 */
export const formatDate = (arg, formatter = 'YYYY-MM-DD') => {
  if (!arg) {
    return ''
  }

  try {
    let result
    if (Array.isArray(arg)) {
      result = []
      arg.forEach(item => {
        const date = formatDate(item, formatter)
        result.push(date)
      })
    } else if (moment.isMoment(arg)) {
      result = arg.format(formatter)
    } else if (typeof arg === 'object') {
      if (arg instanceof Date) {
        const date = moment(arg)
        result = date.format(formatter)
      } else {
        const { year, month, day } = arg
        const date = new Date()
        date.setFullYear(year)
        date.setMonth(month - 1)
        date.setDate(day)
        result = moment(date).format(formatter)
      }
    } else {
      const date = moment(arg)
      result = date.format(formatter)
    }
    return result
  } catch (e) {
    console.log(e)
    return ''
  }
}

export function validateForm(ref) {
  return new Promise(resolve => {
    if (!ref) {
      return resolve(false)
    }

    ref.validate(valid => {
      resolve(valid)
      if (!valid) {
        return false
      }
    })
  })
}
