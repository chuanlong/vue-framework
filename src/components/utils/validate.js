/**
 * @param {String} arg
 * @returns {boolean}
 */
export function validPhone(arg) {
  const reg = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
  return reg.test(arg)
}

/**
 * @param {String} arg
 * @returns {boolean}
 */
export function validIdNo(arg) {
  const reg = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|31)|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/
  return reg.test(arg)
}

/**
 * transform form's validation
 * @param ref : element form's ref
 * @returns {Promise<Boolean>}
 */
export const validateForm = async ref => {
  return new Promise(resolve => {
    if (!ref) {
      return resolve(false)
    }

    try {
      ref.validate(valid => {
        resolve(valid)
        if (!valid) {
          return false
        }
      })
    } catch (e) {
      return false
    }
  })
}
