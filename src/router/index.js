import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    noPermCtrl: true,
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    noPermCtrl: true,
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    noPermCtrl: true,
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    noPermCtrl: true,
    component: () => import('@/views/error-page/404'),
    // hidden: true
  },
  {
    path: '/401',
    noPermCtrl: true,
    component: () => import('@/views/error-page/401'),
    // hidden: true
  },

  {
    path: '/',
    noPermCtrl: true,
    redirect: '/index',
    component: Layout,
    name: 'home',
    meta: {
      title: '首页',
      icon: 'iconIcon_shouye'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/home/index'),
        name: 'HomePage',
        meta: { title: '首页', icon: 'iconIcon_shouye', affix: true, noCache: true }
      },
      {
        path: 'list',
        component: () => import('@/views/home/list'),
        name: 'HomeList',
        meta: { title: '全部待办列表' },
        hidden: true
      }
    ]
  },
  {
    code: 'COMPONENTS',
    path: '/components',
    component: Layout,
    alwaysShow: true,
    meta: {
      title: '组件',
      icon: 'iconicon_ziyuanguanli'
    },
    children: [
      {
        code: 'kindeditor',
        path: 'kindeditor',
        name: 'kindeditor',
        component: () => import('@/views/components/kindeditor'),
        meta: {
          title: '富文本编辑器(kindeditor)',
        }
      },
      {
        code: 'tinymce',
        path: 'tinymce',
        name: 'tinymce',
        component: () => import('@/views/components/tinymce'),
        meta: {
          title: '富文本编辑器(Tinymce)'
        }
      },
      {
        code: 'jsoneditor',
        path: 'jsoneditor',
        name: 'jsoneditor',
        component: () => import('@/views/components/jsoneditor'),
        meta: {
          title: 'JSONEditor'
        }
      },
      {
        code: 'dropzone',
        path: 'dropzone',
        name: 'dropzone',
        component: () => import('@/views/components/dropzone'),
        meta: {
          title: '拖拽上传'
        }
      },
      {
        code: 'count-to',
        path: 'count-to',
        name: 'countTo',
        component: () => import('@/views/components/count-to'),
        meta: {
          title: 'CountTo'
        }
      },
      {
        code: 'draggableDialog',
        path: 'draggableDialog',
        name: 'draggableDialog',
        component: () => import('@/views/components/draggable-dialog'),
        meta: {
          title: '可拖动对话框'
        }
      },
      {
        code: 'dndlist',
        path: 'dndlist',
        name: 'dndlist',
        component: () => import('@/views/components/dndlist'),
        meta: {
          title: '可拖动列表'
        }
      },
      {
        code: 'kanban',
        path: 'kanban',
        name: 'kanban',
        component: () => import('@/views/components/kanban'),
        meta: {
          title: '看板'
        }
      },
      {
        code: 'listpage',
        path: 'listpage',
        name: 'ListPage',
        component: () => import('@/views/components/listpage'),
        meta: {
          title: 'ListPage'
        }
      },
      {
        code: 'formcard',
        path: 'formcard',
        name: '表单',
        component: () => import('@/views/components/form-card'),
        meta: {
          title: '表单'
        }
      },
      {
        code: 'table-select',
        path: 'table-select',
        name: '表格选择',
        component: () => import('@/views/components/table-select'),
        meta: {
          title: '表格选择'
        }
      },
      {
        code: 'description',
        path: 'description',
        name: 'Description',
        component: () => import('@/views/components/description'),
        meta: {
          title: 'Description'
        }
      }
    ]
  },
  {
    code: 'CHARTS',
    path: '/charts',
    component: Layout,
    name: 'CHARTS',
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    meta: {
      title: '图表',
      icon: 'iconicon_chanpinguanlizhongxin'
    },
    children: [
      {
        code: 'Column',
        path: 'column',
        component: () => import('@/views/charts/column'),
        name: '柱状图',
        meta: { title: '柱状图', icon: '', noCache: false }
      },
      {
        code: 'line',
        path: 'line',
        component: () => import('@/views/charts/line'),
        name: '柱状图',
        meta: { title: '折线图', icon: '', noCache: false }
      }
    ]
  },
  {
    code: 'PRODUCTS_MGT',
    path: '/products',
    component: Layout,
    name: 'ProductsMgt',
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    meta: {
      title: '产品管理中心',
      icon: 'iconicon_chanpinguanlizhongxin'
    },
    children: [
      {
        code: 'SCENIC_MGT',
        path: 'scenic_area',
        alwaysShow: true,
        redirect: 'noRedirect',
        name: 'products',
        component: () => import('@/views/products/product-container'),
        meta: {
          title: '景区产品管理'
        },
        children: [
          {
            code: 'SCENIC_LIST',
            path: 'scenic_area_list',
            component: () => import('@/views/products/scenic-area-mgnt/scenic-area-list'),
            name: 'ScenicAreaList',
            meta: { title: '景区列表', icon: '', noCache: false }
          }
        ]
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
