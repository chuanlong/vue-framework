import { updateToDoList } from '@/api/home'

export const typeListMap = {// 渠道商状态
  '': '全部',
  0: '定价方案审核',
  1: '订单审核',
  2: '预付款充值',
  3: '渠道商授信',
  4: '渠道商负充'
}

export const auditStatusMap = {
  '': '全部',
  0: '待处理',
  1: '已处理'
}

export const contentTypeMap = {
  0: '消息',
  1: '待办'
}

export const jumpPage = async function(row, _t) {
  const { type, contentType, businessNo, id } = row // contentType 0消息 1待办
  if (!businessNo || businessNo === 'null') return
  let url = ''
  let actionType = ''
  if (type === '0') { // 定价方案审核
    url = '/pricing/detail'
    actionType = 'audit'
  }
  if (type === '2') { // 预付款充值
    url = '/merchant/prepayment/recharge/list'
  }
  if (type === '3') { // 渠道商授信
    url = '/merchant/prepayment/credit/list'
  }
  if (type === '4') { // 渠道商负充
    url = '/merchant/prepayment/discharge/list'
  }
  if (contentType === '0' && id) { // 消息更新状态
    try {
      await updateToDoList(id)
    } catch (error) {
      console.log('xxxxx updateToDoList: error ==>', error)
    }
  }
  if (url) {
    url = `${url}?id=${businessNo}&disabled=${true}&type=${actionType}`
    _t.$router.push(url)
  }
}
