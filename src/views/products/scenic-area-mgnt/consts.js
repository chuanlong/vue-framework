export const officeStatusMap = {
  '0': '未营业',
  '1': '营业中'
}
export const gateTypeMap = {
  '0': '三辊闸',
  '1': '翼闸',
  '2': '摆闸',
  '3': '平移闸'
}
