export const hotelStatusMap = {
  0: '未营业',
  1: '营业中'
}

export const hotelProductStatusMap = {
  0: '禁用',
  1: '启用'
}

export const houseLayoutStatusMap = {// 房型状态
  0: '禁用',
  1: '启用'
}

export const facilityFields = [
  {
    zh: 'WI-FI',
    en: 'WI-FI'
  },
  {
    zh: '高尔夫',
    en: 'Golf'
  },
  {
    zh: '健身房',
    en: 'Gym'
  },
  {
    zh: '停车场',
    en: 'Parking'
  },
  {
    zh: '接送机',
    en: 'Airport Transfer'
  },
  {
    zh: '游泳池',
    en: 'Swimming'
  },
  {
    zh: 'ATM取款机',
    en: 'ATM'
  },
  {
    zh: '电影院',
    en: 'Cinema'
  },
  {
    zh: '餐厅',
    en: 'Restaurant'
  },
  {
    zh: 'SPA',
    en: 'SPA'
  },
  {
    zh: '会议',
    en: 'Meeting'
  },
  {
    zh: 'KTV',
    en: 'KTV'
  },
  {
    zh: '儿童欢乐中心',
    en: 'Children Park'
  }
]
