import request from '@/utils/request'

export function scenicList(data) { // 景区列表
  return request({
    url: 'scenic/queryList',
    method: 'POST',
    data
  })
}

export function ticketList(data) { // 票类列表
  return request({
    url: 'scenic/ticket/queryList',
    method: 'POST',
    data
  })
}

export function scenicAdd(data) { // 新增景区
  return request({
    url: 'scenic/create',
    method: 'POST',
    data
  })
}

export function scenicUpdate(data) { // 编辑景区
  return request({
    url: 'scenic/update',
    method: 'POST',
    data
  })
}

export function getScenicDetail(id) { // 获取景区详情
  return request({
    url: `scenic/detail/${id}`,
    method: 'GET'
  })
}

export function getCatalogs() {
  return request({
    url: 'catalog/1001',
    method: 'GET'
  })
}
