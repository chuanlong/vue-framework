import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/resource/list',
    method: 'get',
    params
  })
}
