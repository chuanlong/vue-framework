import request from '@/utils/request'

export function queryCommonFunction() { // 查询用户常用功能
  return request({
    url: 'function/queryCommonFunction',
    method: 'POST'
  })
}

export function queryResources() { // 获取当前用户的资源列表树
  return request({
    url: 'function/queryResources',
    method: 'POST',
    data: { functionId: window.localStorage.getItem('resourceIds') }
  })
}

export function updateCommonFunction(ids) { // 编辑用户常用功能
  return request({
    url: 'function/updateCommonFunction',
    method: 'POST',
    data: { functionId: ids }
  })
}

export function getToDoList(data) { // 查询待办事项
  return request({
    url: 'todolist/query',
    method: 'POST',
    data
  })
}

export function updateToDoList(id) {
  return request({
    url: `todolist/update/${id}`,
    method: 'GET'
  })
}
