import request from '@/utils/request'

export function deleteHoliday(id) { // 删除节假日
  return request({
    url: `holiday?id=${id}`,
    method: 'DELETE'
  })
}

export function insertOrUpdate(data) { // 新增或修改节假日
  return request({
    url: 'holiday',
    method: 'POST',
    data
  })
}

export function getDetail(id) { // 查询节假日详情
  return request({
    url: 'holiday',
    method: 'GET',
    params: { id }
  })
}
