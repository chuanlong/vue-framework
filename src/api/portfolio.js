import request from '@/utils/request'

export function createPortfolio(data) {
  return request({
    url: `groupItem/create`,
    method: 'POST',
    data
  })
}

export function updatePortfolio(data) {
  return request({
    url: `groupItem/update`,
    method: 'PUT',
    data
  })
}

export function getPortfolioDetail(id) {
  return request({
    url: `groupItem/detail/${id}`,
    method: 'GET'
  })
}
