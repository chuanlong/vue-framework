import request from '@/utils/request'

export function addLimitSellRule(data) {
  return request({
    url: 'limit-sell-rule',
    method: 'POST',
    data
  })
}

export function editLimitSellRule(data) {
  return request({
    url: 'limit-sell-rule',
    method: 'PUT',
    data
  })
}

export function getLimitSellRuleDetail(id) {
  return request({
    url: 'limit-sell-rule/detail',
    method: 'GET',
    params: { id }
  })
}
