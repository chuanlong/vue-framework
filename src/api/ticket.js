import request from '@/utils/request'

export function ticketAdd(data) { // 新增票类
  return request({
    url: 'scenic/ticket/create',
    method: 'POST',
    data
  })
}

export function ticketUpdate(data) { // 编辑票类
  return request({
    url: 'scenic/ticket/update',
    method: 'POST',
    data
  })
}

export function batchSetScenic(data) { // 批量设置票类与景区关联
  return request({
    url: 'scenic/ticket/batchSetScenic',
    method: 'POST',
    data
  })
}

export function queryMaterial(data) { // 物料
  return Promise.resolve({
    'extFields': {},
    'exceptCauseIp': null,
    'exceptCauseApp': null,
    'exceptClass': null,
    'resultCode': '0',
    'resultMsg': 'success',
    'data': [{
      'extFields': {},
      'matnr': '10000140',
      'maktx': '呲水枪',
      'werks': '1000',
      'matkl': '11010601',
      'mtart': 'Z001'
    }]
  })
  // return request({
  //   url: 'product/sap/queryMaterial',
  //   method: 'POST',
  //   data
  // })
}

export function getTicketDetail(id) { // 获取票类详情
  return request({
    url: `scenic/ticket/detail/${id}`,
    method: 'GET'
  })
}
