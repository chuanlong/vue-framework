
import request from '@/utils/request'

export function login(data) {
  return request({
    url: `auth/login?verifyCode=` + data.captcha,
    method: 'POST',
    data: { username: data.username, password: data.password },
    headers: {
      'verify-uniqueId': data.uniqueId
    }
  })
}

export function logout() { // auth/logout
  return request({
    url: `/auth/logout`,
    method: 'PUT'
  })
}

