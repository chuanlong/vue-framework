import request from '@/utils/request'

export function getHotelDetail(id) {
  return request({
    url: `hotel/detail/${id}`,
    method: 'GET',
    params: {}
  })
}

export function getRelatedHouseTypes(hotelId) {
  return request({
    url: `hotel/layout/queryList`,
    method: 'GET',
    params: { hotelIds: hotelId, pageNum: 1, pageSize: 1000 }
  })
}

export function getRelatedHotelProducts(hotelId) {
  return request({
    url: `hotel/product/queryList`,
    method: 'GET',
    params: { hotelIds: hotelId, pageNum: 1, pageSize: 1000 }
  })
}

export function createHotel(data) {
  return request({
    url: `hotel/create`,
    method: 'POST',
    data
  })
}

export function updateHotel(data) {
  return request({
    url: `hotel/update`,
    method: 'POST',
    data
  })
}

export function getHotelProductDetail(id) {
  return request({
    url: `hotel/product/detail/${id}`,
    method: 'GET'
  })
}

export function addHotelProduct(data) {
  return request({
    url: `hotel/product/create`,
    method: 'POST',
    data
  })
}

export function updateHotelProduct(data) {
  return request({
    url: `hotel/product/update`,
    method: 'POST',
    data
  })
}

export function addHotelHouseType(data) {
  return request({
    url: `hotel/layout/create`,
    method: 'POST',
    data
  })
}

export function updateHotelHouseType(data) {
  return request({
    url: `hotel/layout/update`,
    method: 'POST',
    data
  })
}
export function getHouseTypeDetail(id) {
  return request({
    url: `hotel/layout/detail/${id}`,
    method: 'GET'
  })
}

export function houseTypeRelateHotel(data) {
  return request({
    url: `hotel/layout/relateHotel`,
    method: 'POST',
    data
  })
}

export function delRelateHotel(data) {
  return request({
    url: `hotel/layout/delRelateHotel`,
    method: 'DELETE',
    data
  })
}

export function getHotelCrsCodes() {
  return request({
    url: `hotel/queryCrsCodeList`,
    method: 'GET'
  })
}

export function getHotels(data) {
  return request({
    url: `hotel/queryList`,
    method: 'POST',
    data: {
      ...data,
      pageNum: 1,
      pageSize: 10000
    }
  })
}

export function getLayoutCrsCodes(params) {
  return request({
    url: `hotel/layout/queryCrsCodeList`,
    method: 'GET',
    params
  })
}
