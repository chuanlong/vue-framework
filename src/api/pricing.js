import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/vue-element-admin/pricing/list',
    method: 'post',
    data
  })
}

export function getDetail(id) {
  return request({
    url: '/vue-element-admin/pricing/detail',
    method: 'get',
    params: { id }
  })
}
