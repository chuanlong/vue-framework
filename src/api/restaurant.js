import request from '@/utils/request'

export function getRestaurants(data) {
  return request({
    url: `catering/restaurant/queryList`,
    method: 'POST',
    data: {
      ...data,
      pageNum: 1,
      pageSize: 10000
    }
  })
}

export function getDetail(id) {
  return request({
    url: `catering/restaurant/queryDetail/${id}`,
    method: 'GET',
    params: {}
  })
}

export function create(data) {
  return request({
    url: `catering/restaurant/create`,
    method: 'POST',
    data
  })
}

export function update(data) {
  return request({
    url: `catering/restaurant/update`,
    method: 'PUT',
    data
  })
}
