import request from '@/utils/request'

export function getHolidays(year) {
  return request({
    url: `holiday/list`,
    method: 'POST',
    data: year
  })
}
export function getOrg() {
  return request({
    url: `dept/list`,
    method: 'GET',
    params: {}
  })
}


export function getCommercialType() {
  return request({
    url: 'commercial-type',
    method: 'GET',
    params: {}
  })
}

export function getMaterials(data) {
  return request({
    url: 'product/sap/queryMaterial',
    method: 'POST',
    data
  })
}

export function getSAPList(data) {
  return request({
    url: 'product/sap/queryStoreCustomer',
    method: 'POST',
    data
  })
}
