import request from '@/utils/request'

export function getInfoList(data) {
  return request({
    url: '/vue-element-admin/merchant/info/list',
    method: 'post',
    data
  })
}

export function getInfoDetail(id) {
  return request({
    url: '/vue-element-admin/merchant/info/detail',
    method: 'get',
    params: { id }
  })
}

export function getAccountList(data) {
  return request({
    url: '/vue-element-admin/merchant/account/list',
    method: 'post',
    data
  })
}

export function getGroupList(data) {
  return request({
    url: '/vue-element-admin/merchant/group/list',
    method: 'post',
    data
  })
}

export function getGroupDetail(data) {
  return request({
    url: '/vue-element-admin/merchant/group/detail',
    method: 'post',
    data
  })
}

export function getGroupAddList(data) {
  return request({
    url: '/vue-element-admin/merchant/group/add/list',
    method: 'post',
    data
  })
}

export function getPrepaymentList(data) {
  return request({
    url: '/vue-element-admin/merchant/prepayment/list',
    method: 'post',
    data
  })
}
