import config from './config'

const TokenKey = config.localStorage.auth_name
const PermKey = config.localStorage.dataPermission

export function getToken() {
  if (config.isLogin) {
    return localStorage.getItem(TokenKey)
  }
  return true
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, token)
}

export function removeToken() {
  localStorage.removeItem(config.localStorage.roles)
  localStorage.removeItem(config.localStorage.resourceIds)
  localStorage.removeItem(config.localStorage.resources)
  localStorage.removeItem(config.localStorage.dataPermission)
  return localStorage.removeItem(TokenKey)
}

export function getPerm() {
  if (config.isLogin) {
    const string = localStorage.getItem(PermKey)
    return string ? string.split(',') : null
  }
  return null
}

export function setPerm(string) {
  return localStorage.setItem(PermKey, string)
}
