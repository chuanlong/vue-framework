import config from './config'

export function checkBtnPermByCode(code) {
  try {
    const resourceList = JSON.parse(localStorage.getItem(config.localStorage.resources) || '[]')
    return resourceList.some(item => {
      if (item.resourceCode === code) {
        return true
      }
    })
  } catch (e) {
    console.log('parse resource error:', e)
  }
}
