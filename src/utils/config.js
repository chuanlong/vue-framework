
const config = {
  successResultCode: 0,
  ENV: process.env.VUE_APP_ENV,
  cookie: {},
  localStorage: {
    account: 'account',
    nick_name: 'nickName',
    roles: 'roles',
    resourceIds: 'resourceIds',
    dataPermission: 'dataPermission',
    resources: 'resources',
    loginTime: 'loginTime',
    deptName: 'deptName',
    deptId: 'deptId',
    groupId: 'groupId',
    ipAddress: 'ipAddress',
    employeeName: 'employeeName',
    auth_name: 'auth-token'
  },
  isLogin: true// 是否校验登录状态
}
const envConfig = {
  loc: {
    host: 'http://10.128.98.74',
    // host: 'http://10.128.98.79',
    // host: 'http://10.128.98.87',
    // https://hyompm.evergrande.com
    app: 'hd-operation-application',
    mock: true
  },
  development: {
    host: '',
    app: 'hd-operation-application',
    mock: false
  },
  test: {
    host: '',
    app: 'hd-operation-application',
    mock: false
  },
  production: {
    host: '',
    app: 'hd-operation-application',
    mock: false
  },
  staging: {
    host: '',
    app: 'hd-operation-application',
    mock: false
  }
}
Object.assign(config, envConfig[config.ENV])

config.getBaseUrl = function() {
  const { ENV, host, app, mock } = config
  const version = 'v1'
  if (ENV === 'loc' && mock) {
    return process.env.VUE_APP_BASE_API
  }
  return `${host}/${app}/${version}`
}
export default config
