import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import siteConfig from './config'

// create an axios instance
const service = axios.create({
  baseURL: siteConfig.getBaseUrl(), // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 30 * 1000 // request timeout, millisecond
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    // console.log(config,'request.config')
    let headers = config.headers
    // 解决ie不重新请求问题
    headers = {
      ...headers,
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': -1,
      'auth-token': store.getters.token ? getToken() : ''
    }
    if (!headers['Content-Type']) {
      headers['Content-Type'] = 'application/json'
    }
    config.headers = headers
    config.responseType = 'json'
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not <var>config.successResultCode</var>, it is judged as an error.
    if ('' + res.resultCode !== '' + siteConfig.successResultCode) {
      Message({
        message: res.resultMsg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(new Error(res.resultMsg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    if (error.response && error.response.status === 401) {
      store.dispatch('user/resetToken').then(() => {
        location.reload()
      })
    } else {
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  }
)

export default service
