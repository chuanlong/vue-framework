import moment from 'moment'

export function duplicateRulesOfEn(rules, ...props) {
  if (!rules || !props || !props.length) {
    return
  }

  let formItemList = []
  props && props.forEach(prop => {
    formItemList = prop.formItemList ? formItemList.concat(prop.formItemList) : formItemList
  })
  const obj = {}
  Object.keys(rules).forEach(key => {
    const index = formItemList ? formItemList.findIndex(field => field.prop === key) : -1
    const field = index < 0 ? null : formItemList[index]
    const en_prop = field ? field.en_prop : null

    if (en_prop) {
      obj[en_prop] = rules[key]
    }
  })
  Object.assign(rules, obj)
}

/**
 * @author yjl
 * @date 2020/3/27
 * @param {*} params
 * @param {*} clear
 * @description filter undefined, null and blank string
 */
export function clearParams(params, clear) {
  if (!params) {
    return null
  }

  const obj = {}
  const keys = Object.keys(params)
  keys.forEach(key => {
    const val = params[key]
    let isNil
    if (clear && typeof clear === 'function') {
      isNil = clear(val)
    } else {
      isNil = val === undefined || val === null || `${val}`.trim() === ''
    }
    if (!isNil) {
      obj[key] = val
    }
  })
  return obj
}

/**
 * 格式化日期
 * @param arg
 *        string   'YYYY-MM'/'YYYY-MM-DD'/'YYYY-MM-DD hh:mm:ss'
 *        moment
 *        Date
 *        object  { year, month, day }
 * @param formatter
 * @returns {string|*[]}
 */
export const formatDate = (arg, formatter = 'YYYY-MM-DD') => {
  if (!arg) {
    return ''
  }

  try {
    let result
    if (Array.isArray(arg)) {
      result = []
      arg.forEach(item => {
        const date = formatDate(item, formatter)
        result.push(date)
      })
    } else if (moment.isMoment(arg)) {
      result = arg.format(formatter)
    } else if (typeof arg === 'object') {
      if (arg instanceof Date) {
        const date = moment(arg)
        result = date.format(formatter)
      } else {
        const { year, month, day } = arg
        const date = new Date()
        date.setFullYear(year)
        date.setMonth(month - 1)
        date.setDate(day)
        result = moment(date).format(formatter)
      }
    } else {
      const date = moment(arg)
      result = date.format(formatter)
    }
    return result
  } catch (e) {
    console.log(e)
    return ''
  }
}

export function validateForm(ref) {
  return new Promise(resolve => {
    if (!ref) {
      return resolve(false)
    }

    ref.validate(valid => {
      resolve(valid)
      if (!valid) {
        return false
      }
    })
  })
}

export const unsignAmount = arg => {
  if (!arg) {
    return ''
  }

  let result = `${arg}`.replace('-', '')
  result = `${result}`.replace('+', '')

  return result
}

export const searchIdsAtTree = (keyword, tree, match = 'name', id = 'id', children = 'children') => {
  if (!tree || !tree.length || !keyword) {
    return []
  }

  const _func = (value, path) => {
    if (!path) {
      return
    }

    if (typeof path === 'function') {
      return path(value)
    }

    const array = path.split('.')
    let result = value
    array.forEach(item => (result = result[item]))
    return result
  }

  let results = []
  tree && tree.forEach(item => {
    const valMatch = _func(item, match)
    const index = valMatch.indexOf(keyword)
    if (index >= 0) {
      const valId = _func(item, id)
      results.push(valId)
    }
    const valChildren = _func(item, children)
    const arr = searchIdsAtTree(keyword, valChildren, match, id, children)
    results = arr ? results.concat(arr) : results
  })

  return results
}

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function getQueryObject(url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

/**
 * @param {string} input value
 * @returns {number} output value
 */
export function byteLength(str) {
  // returns the byte length of an utf8 string
  let s = str.length
  for (var i = str.length - 1; i >= 0; i--) {
    const code = str.charCodeAt(i)
    if (code > 0x7f && code <= 0x7ff) s++
    else if (code > 0x7ff && code <= 0xffff) s += 2
    if (code >= 0xDC00 && code <= 0xDFFF) i--
  }
  return s
}

/**
 * @param {Array} actual
 * @returns {Array}
 */
export function cleanArray(actual) {
  const newArray = []
  for (let i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i])
    }
  }
  return newArray
}

/**
 * @param {Object} json
 * @returns {Array}
 */
export function param(json) {
  if (!json) return ''
  return cleanArray(
    Object.keys(json).map(key => {
      if (json[key] === undefined) return ''
      return encodeURIComponent(key) + '=' + encodeURIComponent(json[key])
    })
  ).join('&')
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

/**
 * @param {string} val
 * @returns {string}
 */
export function html2Text(val) {
  const div = document.createElement('div')
  div.innerHTML = val
  return div.textContent || div.innerText
}

/**
 * Merges two objects, giving the last one precedence
 * @param {Object} target
 * @param {(Object|Array)} source
 * @returns {Object}
 */
export function objectMerge(target, source) {
  if (typeof target !== 'object') {
    target = {}
  }
  if (Array.isArray(source)) {
    return source.slice()
  }
  Object.keys(source).forEach(property => {
    const sourceProperty = source[property]
    if (typeof sourceProperty === 'object') {
      target[property] = objectMerge(target[property], sourceProperty)
    } else {
      target[property] = sourceProperty
    }
  })
  return target
}

/**
 * @param {HTMLElement} element
 * @param {string} className
 */
export function toggleClass(element, className) {
  if (!element || !className) {
    return
  }
  let classString = element.className
  const nameIndex = classString.indexOf(className)
  if (nameIndex === -1) {
    classString += '' + className
  } else {
    classString =
      classString.substr(0, nameIndex) +
      classString.substr(nameIndex + className.length)
  }
  element.className = classString
}

/**
 * @param {string} type
 * @returns {Date}
 */
export function getTime(type) {
  if (type === 'start') {
    return new Date().getTime() - 3600 * 1000 * 24 * 90
  } else {
    return new Date(new Date().toDateString())
  }
}

/**
 * @param {Function} func
 * @param {number} wait
 * @param {boolean} immediate
 * @return {*}
 */
export function debounce(func, wait, immediate) {
  let timeout, args, context, timestamp, result

  const later = function() {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔 last 小于设定时间间隔 wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function(...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}

/**
 * This is just a simple version of deep copy
 * Has a lot of edge cases bug
 * If you want to use a perfect deep copy, use lodash's _.cloneDeep
 * @param {Object} source
 * @returns {Object}
 */
export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * @param {Array} arr
 * @returns {Array}
 */
export function uniqueArr(arr) {
  return Array.from(new Set(arr))
}

/**
 * @returns {string}
 */
export function createUniqueString() {
  const timestamp = +new Date() + ''
  const randomNum = parseInt((1 + Math.random()) * 65536) + ''
  return (+(randomNum + timestamp)).toString(32)
}

/**
 * Check if an element has a class
 * @param {HTMLElement} elm
 * @param {string} cls
 * @returns {boolean}
 */
export function hasClass(ele, cls) {
  return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

/**
 * Add class to element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function addClass(ele, cls) {
  if (!hasClass(ele, cls)) ele.className += ' ' + cls
}

/**
 * Remove class from element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function removeClass(ele, cls) {
  if (hasClass(ele, cls)) {
    const reg = new RegExp('(\\s|^)' + cls + '(\\s|$)')
    ele.className = ele.className.replace(reg, ' ')
  }
}

// 数字格式化
export function numberFormat(
  number,
  precision = 2,
  needPrefix = false,
  prefix = '￥'
) {
  // 是否需要币种标识
  const displayPrefix = needPrefix ? prefix : ''
  // 去除特殊字符
  number = String(number).replace(/(^\s*)|(\s*$)/g, '')
  if (isNaN(number) || !number) {
    return displayPrefix + parseFloat(0).toFixed(precision)
  } else {
    number = parseFloat(number).toFixed(precision)
  }
  // 转换为字符串格式，方便下步操作
  number = number + ''
  if (number) {
    // 数据截取，分割成整数、小数
    const nums = number.split('.')
    let integer = nums[0]
    const decimals = nums[1]
    let isDecimals = false
    // 判断是否为负数，并做额外处理（获取不包含负数的数位、并设置负数开关）
    if (integer.indexOf('-') > -1) {
      integer = integer.slice(integer.indexOf('-') + 1)
      isDecimals = true
    }
    // 获取当前整数中，为3的倍数的数位 --- 如：1234 -> 234
    const num = integer.slice(integer.length % 3)
    // 获取当前整数中，不为3的倍数的数位 --- 如：1234 -> 1
    const numBegin = integer.slice(0, integer.length % 3)
    // 格式处理
    number =
      (isDecimals ? '-' : '') +
      numBegin +
      (numBegin && num ? ',' : '') +
      (num ? num.match(/\d{3}/g).join(',') : '') +
      (decimals ? '.' + decimals : '')
  }
  return displayPrefix + number
}
