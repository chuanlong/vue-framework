export const languages = [
  {
    label: '中文',
    value: 'zh'
  },
  {
    label: '英文',
    value: 'en'
  }
]
